﻿using Microsoft.Extensions.Configuration;
using WL.Think.FileUploader;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class Extension
    {
        public static IServiceCollection AddFileUploader(this IServiceCollection services,IConfiguration configuration)
        {
            services.AddTransient<IFileUploader, FileUploader>();
            services.AddOptions<FileUploadOption>();
            services.Configure<FileUploadOption>(configuration.GetSection("FileUploader"));
            return services;
        }
    }
}
