# WL.Think.FileUploader

#### 添加配置

在 `appsetting.json` 上添加配置
```json
{
    "FileUploader": {
        "FileSizeLimit": 10485760,
        "SupportFiles": [
            ".txt",
            ".csv",
            ".jpg"
        ]
     }
}
```


#### 添加服务

```
services.AddFileUploader(Configuration)
```

#### 使用

1. 在控制器中注入 `IFileUploader` 服务
2. 在对应的Action方法中注入特性 `DisableFormValueModelBindingAttribute`
3. 然后调用 `IFileUploader.Save` 等方法

