﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WL.Think.FileUploader
{
    public class FileUploadOption
    {
        /// <summary>
        /// 文件限制大小
        /// </summary>
        public long FileSizeLimit { get; set; } = 10485760;

        /// <summary>
        /// 支持的文件和签名值，用来做文件的判断
        /// </summary>
        public List<string> SupportFiles { get; set; }
    }
}
