﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WL.Think.FileUploader
{
    /// <summary>
    /// 文件上传器
    /// </summary>
    public interface IFileUploader
    {
        /// <summary>
        /// 保存到目标流中
        /// </summary>
        /// <param name="request"></param>
        /// <param name="targetStream">可以是文件等其他数据流</param>
        Task<FormValueProvider> Save(HttpRequest request, Stream targetStream);

        /// <summary>
        /// 保存到目标目录
        /// </summary>
        /// <param name="request"></param>
        /// <param name="targetDirectory"></param>
        Task<FormValueProvider> Save(HttpRequest request, string targetDirectory);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="formFile"></param>
        /// <returns></returns>
        Task<byte[]> Save(IFormFile formFile);


        /// <summary>
        /// 保存到指定目录
        /// </summary>
        /// <param name="formFile"></param>
        /// <param name="targetDirectory"></param>
        /// <returns></returns>
        Task Save(IFormFile formFile, string targetDirectory);
    }
}
